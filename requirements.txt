# Available in stretch-backports
Django>=1.11.7,<2.0

# Available in Debian
PyYAML
django-countries
psycopg2
python-memcached

# From the cheeseshop
django-crispy-forms>=1.7.0
django-paypal==0.4.1
mdx_linkify==1.2
wafer==0.7.5
wafer-debconf==0.1.21

# unused but a dependency of django-bakery
# pinned to avoid having to update it every day
boto3==1.9.119
botocore==1.12.119
