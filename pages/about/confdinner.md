---
name: Conference Dinner
---
# Conference Dinner

Every year, we celebrate DebConf with a special dinner.

## When?

FIXME

## Where?

FIXME

## Cost

If you have a food bursary, the conference dinner is included.

If you are self-paying for food, the conference dinner will cost FIXME.

## Registration

Select the conference dinner during registration.
If appropriate, the cost will be added to your bill.
Pay online, or at the front-desk.

## Food

To be defined.

## Drinks

To be defined.

### Beer

To be defined.

### Wine

To be defined.

### Non-alcoholic

To be defined.
